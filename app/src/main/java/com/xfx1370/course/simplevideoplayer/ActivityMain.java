package com.xfx1370.course.simplevideoplayer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

public class ActivityMain extends AppCompatActivity{

   private static final int READ_REQUEST_CODE = 1;
   private static final String TAG = "TAG";
   Button btnOpen;
   VideoView vdwVideo;
   Uri currentVideoUri;
   int videoPosition;
   @Override
   protected void onCreate(Bundle savedInstanceState){
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
      btnOpen = (Button) findViewById(R.id.btnOpen);
      vdwVideo = (VideoView) findViewById(R.id.vdwVideo);
      MediaController mediaController = new MediaController(this);
      mediaController.setAnchorView(vdwVideo);
      vdwVideo.setMediaController(new MediaController(this));

      if(savedInstanceState!=null){
         currentVideoUri= savedInstanceState.getParcelable("currentVideoUri");
         videoPosition= savedInstanceState.getInt("videoPosition");
      }

      btnOpen.setOnClickListener(new View.OnClickListener(){
         @Override
         public void onClick(View view){
            browseStorage();
         }
      });
   }

   private void browseStorage(){
      Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
      intent.addCategory(Intent.CATEGORY_OPENABLE);
      intent.setType("video/*");
      startActivityForResult(intent, READ_REQUEST_CODE);

   }

   @Override
   public void onSaveInstanceState(Bundle savedInstanceState){
      super.onSaveInstanceState(savedInstanceState);
      savedInstanceState.putInt("videoPosition", vdwVideo.getCurrentPosition());
      savedInstanceState.putParcelable("currentVideoUri", currentVideoUri);
   }

   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent resultData){

      if(requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK){
         Uri uri = null;
         if(resultData != null){
            uri = resultData.getData();
            Log.i(TAG, "Uri: " + uri.toString());
            playVideo(uri);
         }
      }
   }

   private void playVideo(Uri uri){
      currentVideoUri = uri;
      vdwVideo.setVideoURI(currentVideoUri);
      vdwVideo.requestFocus();
      vdwVideo.start();
   }

   @Override
   protected void onPause(){
      super.onPause();
      videoPosition =vdwVideo.getCurrentPosition();
   }

   @Override
   protected void onResume(){
      super.onResume();
      Log.i(TAG, "Uri: " + currentVideoUri);

      if(currentVideoUri != null){
         Log.i(TAG," videoPosition = "+ videoPosition);
         vdwVideo.setVideoURI(currentVideoUri);
         vdwVideo.seekTo(videoPosition);
         vdwVideo.start();
      }
   }
}
